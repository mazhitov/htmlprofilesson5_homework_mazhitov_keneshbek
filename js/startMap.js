// The following example creates complex markers to indicate beaches near
// Sydney, NSW, Australia. Note that the anchor is set to (0,32) to correspond
// to the base of the flagpole.
function initMap() {
    const map = new google.maps.Map(document.getElementById("map"), {
        zoom: 4,
        center: {lat: 43.38376642273823, lng: 0.5662900374344516},
    });
    setMarkers(map);
}

// Data for the markers consisting of a name, a LatLng and a zIndex for the
// order in which these markers should display on top of each other.
const beaches = [
    ["Madrid", 40.42062132983539, -3.704266103770723, 4],
    ["turin", 45.07666387162132, 7.668370214419389, 5],
    ["hamburg", 53.50437139502831, 9.90169802126805, 3],
    ["krakow", 50.07489312041875, 19.912167366287058, 2],
];

function setMarkers(map) {
// Adds markers to the map.
// Marker sizes are expressed as a Size of X,Y where the origin of the image
// (0,0) is located in the top left of the image.
// Origins, anchor positions and coordinates of the marker increase in the X
// direction to the right and in the Y direction down.
// Shapes define the clickable region of the icon. The type defines an HTML
//<area> element 'poly' which traces out a polygon as a series of X,Y points.
    // The final coordinate closes the poly by connecting to the first coordinate.
    const shape = {
        coords: [1, 1, 1, 20, 18, 20, 18, 1],
        type: "poly",
    };

    for (let i = 0; i < beaches.length; i++) {
        const beach = beaches[i];

        new google.maps.Marker({
            position: {lat: beach[1], lng: beach[2]},
            map,
            icon: '../images/icons8.png',
            title: beach[0],
            zIndex: beach[3],
        });
    }
}